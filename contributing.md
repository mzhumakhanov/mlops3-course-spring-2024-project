# Rules of contributing

To commit into this repo you have to satisfy `pre-commit` requirements.

Please install `pre-commit` tool in your development environment.  
For the details please consider https://pre-commit.com.  

This repo already contains minimum required configuration of formatters  
and linters, please start using them.

After you installed `pre-commit` tool, you have to initialize pre-commit hooks  
in current repo:  
`pre-commit install`

Now you can start making commits to the repo with git-commands.


## Remarks

During the project development I will be updating contribution rules, so please  
watch out.
