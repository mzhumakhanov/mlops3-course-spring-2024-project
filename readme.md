# Project for MLOps course at ods.ai v3 (spring 2024)

This is a learning project for the MLOps course at ods.ai

By this moment I added formatters and linters configuration into this project.  
Please refer to `contributing.md` for the details.


## How to run dev or prod environments using docker

To build dev environment, you need to clone current repo and build docker image:  
`docker build . -t dev-mlops3 -f Dockerfile-dev`

To build prod env, you need to clone current repo and build docker image:  
`docker build . -t prod-mlops3 -f Dockerfile-prod`

To run docker container with jupyter do the following:   
For dev: `docker run -p 8888:8888 --name dev-mlops3 --rm -u 0 dev-mlops3`  
For prod: `docker run -p 8888:8888 --name prod-mlops3 --rm -u 0 prod-mlops3`  

If you need to mount a directory inside container, use example to run docker container  
as below (choose prod or dev if you need):  
`docker run -p 8888:8888 -v /home/smth:/app/smth --name dev-mlops3 --rm -u 0 dev-mlops3`


## Further notice

All other new details will be added later during the learning process
